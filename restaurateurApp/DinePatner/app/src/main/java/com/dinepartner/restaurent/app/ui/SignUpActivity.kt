package com.dinepartner.restaurent.app.ui

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.dinepartner.restaurent.app.Models.SignUpRequest
import com.dinepartner.restaurent.app.R
import com.dinepartner.restaurent.app.helpers.Constants
import com.google.firebase.FirebaseException
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import com.google.firebase.iid.FirebaseInstanceId
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import com.google.android.gms.tasks.OnCompleteListener
import kotlinx.android.synthetic.main.signup.*

class SignUpActivity : AppCompatActivity() {

    private var name = ""
    private var email = ""
    private var password = ""
    private var contact = ""
    var tokens = " "

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.signup)
        supportActionBar?.title = getString(R.string.join_dine_partner)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
//        firebaseTokenMaking()

        signUp_btn.setOnClickListener {
            if (areInputsOkay()) {
                loadingView.visibility = View.VISIBLE
                PhoneAuthProvider.getInstance().verifyPhoneNumber(
                        contact, 60, TimeUnit.SECONDS, this, otpCallbacks
                )
            }
        }
        loadingView.setOnClickListener { }

    }

    @SuppressLint("StringFormatInvalid")
    private fun firebaseTokenMaking() {
        FirebaseInstanceId.getInstance().instanceId
                .addOnCompleteListener(OnCompleteListener { task ->
                    if (!task.isSuccessful) {
                        Log.w("tag", "getInstanceId failed", task.exception)
                        return@OnCompleteListener
                    }

                    // Get new Instance ID token
                    val token = task.result?.token
                    tokens = token.toString()
                    val editor = getSharedPreferences("tokenSTored", Context.MODE_PRIVATE).edit()
                    editor.putString("token", token)
                    editor.apply()
                    // Log and toast
                    val msg = getString(R.string.msg_token_fmt, token)
                    Log.d("taga", msg)
                    Toast.makeText(baseContext, msg, Toast.LENGTH_SHORT).show()
                })
    }

    private val otpCallbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {


        override fun onVerificationCompleted(p0: PhoneAuthCredential?) {
            loadingView.visibility = View.GONE
            Toast.makeText(this@SignUpActivity, "onVerificationCompleted", Toast.LENGTH_LONG).show()
        }

        override fun onVerificationFailed(p0: FirebaseException?) {
            loadingView.visibility = View.GONE
            Toast.makeText(this@SignUpActivity, "onVerificationFailed", Toast.LENGTH_LONG).show()
        }

        override fun onCodeSent(
                verificationId: String?,
                token: PhoneAuthProvider.ForceResendingToken
        ) {
            loadingView.visibility = View.GONE
            val sdf = SimpleDateFormat("yyyy/MM/dd_HH/mm/ss", Locale.getDefault())
            val currentDateandTime = sdf.format(Date())
            val intent = Intent(this@SignUpActivity, OtpVerificationActivity::class.java)
            val requestData = SignUpRequest(
                    name,
                    email,
                    password,
                    contact,
                    verificationId!!,
                    token,
                    currentDateandTime, tokens
            )
            intent.putExtra(Constants.dataPassKey, requestData)
            startActivity(intent)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        onBackPressed()
        return super.onOptionsItemSelected(item)
    }

    private fun areInputsOkay(): Boolean {
        if (TextUtils.isEmpty(yourName_input.text)) {
            yourName_input.error = getString(R.string.required)
            yourName_input.requestFocus()
            return false
        } else if (TextUtils.isEmpty(emaill_input.text)) {
            emaill_input.error = getString(R.string.required)
            emaill_input.requestFocus()
            return false
        } else if (!Patterns.EMAIL_ADDRESS.matcher(emaill_input.text).matches()) {
            emaill_input.error = getString(R.string.invalid_email)
            emaill_input.requestFocus()
            return false
        } else if (TextUtils.isEmpty(paswd_input.text)) {
            paswd_input.error = getString(R.string.required)
            paswd_input.requestFocus()
            return false
        } else if (paswd_input.text.length < 6) {
            paswd_input.error = getString(R.string.short_password_error)
            paswd_input.requestFocus()
            return false
        } else if (TextUtils.isEmpty(phone_input.text)) {
            phone_input.error = getString(R.string.required)
            phone_input.requestFocus()
            return false
        }
        name = yourName_input.text.toString()
        email = emaill_input.text.toString()
        password = paswd_input.text.toString()
        contact = phone_input.text.toString()
        return true
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }
}