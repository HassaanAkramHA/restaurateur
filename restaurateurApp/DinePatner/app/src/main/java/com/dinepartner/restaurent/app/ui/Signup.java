package com.dinepartner.restaurent.app.ui;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.dinepartner.restaurent.app.R;

public class Signup extends AppCompatActivity implements View.OnClickListener {

    Button SignupButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);


        SignupButton = findViewById(R.id.signUp_btn);
        SignupButton.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.signUp_btn:
                Intent intent = new Intent(Signup.this, Login.class);
                startActivity(intent);
                break;
        }

    }
}
