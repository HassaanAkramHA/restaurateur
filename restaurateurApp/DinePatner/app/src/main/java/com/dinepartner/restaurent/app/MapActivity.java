package com.dinepartner.restaurent.app;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentTransaction;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dinepartner.restaurent.app.R;
import com.dinepartner.restaurent.app.fragments.MapsFragment;
import com.google.gson.Gson;


public class MapActivity extends AppCompatActivity {
    MapsFragment mapFragment = new MapsFragment();
    CardView proceedCard;
    TextView adressTv;
    Toolbar toolbar;
    Intent intent;
    int category;
    String date, time, maids, hours, job_title, email, password;
    String addressOfUserFromMap;
    Gson gson = new Gson();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        email = PreferenceManager.getDefaultSharedPreferences(this).getString("user_email", "email");
        password = PreferenceManager.getDefaultSharedPreferences(this).getString("password", "password");
        initView();


    }

    private void initView() {
        intent = getIntent();
        if (intent != null) {
            category = intent.getIntExtra("category", 0);
            date = intent.getStringExtra("date");
            time = intent.getStringExtra("time");
            maids = intent.getStringExtra("maids");
            hours = intent.getStringExtra("hours");
            job_title = intent.getStringExtra("job_title");
        }

        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.chose_location_frame, mapFragment);

        transaction.replace(R.id.chose_location_frame, mapFragment);
        transaction.commit();
        adressTv = findViewById(R.id.address_from_frag);
        toolbar = findViewById(R.id.toolbar_chose_location);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void getAddressFromFragment(String address, String city, String state, String country) {
        addressOfUserFromMap = address;
        adressTv.setText(address);
    }




}
