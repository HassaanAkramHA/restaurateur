package com.dinepartner.restaurent.app.services

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface EndPoints {

    @GET("api/user/login_restaurant")
    fun loginWithEmail(@Query("email") email: String, @Query("password") password: String): Call<ResponseBody>

    @GET("api/user/login_restaurant")
    fun loginWithMobileNumber(@Query("contact") email: String, @Query("password") password: String): Call<ResponseBody>

    @GET("api/user/signup_restaurant")
    fun signUp(
            @Query("firstName") firstName: String, @Query("lastName") lastName: String, @Query("email") email: String, @Query(
                    "password"
            ) password: String, @Query("contact") contact: String, @Query("created_on") created_on: String, @Query(
                    "token"
            ) token: String
    ): Call<ResponseBody>

    @GET("restaurants/getAllCuisines")
    fun getAllCuisines(): Call<ResponseBody>

    @GET("restaurants/getFeaturedRestaurants")
    fun getFeaturedRestaurants(@Query("cityId") cityId: String): Call<ResponseBody>

    @GET("restaurants/getAllRestaurants")
    fun getAllRestaurants(): Call<ResponseBody>

    @GET("restaurants/search_ajax")
    fun getRestaurantsByOptions(
        @Query("text") name: String, @Query("date") date: String,
        @Query("timings") time: String, @Query("capacity") persons: String
    ): Call<ResponseBody>

    @GET("restaurants/restaurant_images")
    fun getRestaurantImages(@Query("restaurant_slug") restaurantSlug: String): Call<ResponseBody>

    @GET("restaurants/restaurant_reviews")
    fun getRestaurantReview(@Query("restaurant_id") restaurantId: String): Call<ResponseBody>

    @GET("restaurants/restaurant_reviews")
    fun getRestaurantReviews(@Query("restaurant_id") restaurantId: String): Call<ResponseBody>

    @GET("restaurants/getNearByRestaurants")
    fun getNearByRestaurants(@Query("latitude") lat: Double, @Query("longitude") lon: Double): Call<ResponseBody>

    @GET("restaurants/restaurant_menu")
    fun getRestaurantMenu(@Query("restaurant_id") restaurantId: String): Call<ResponseBody>

    @GET("restaurants/getRestaurantDetails")
    fun getRestaurantDetails(@Query("restaurant_id") restaurantId: String): Call<ResponseBody>

    @GET("restaurants/checkbooking")
    fun checkBooking(@QueryMap restaurantId: HashMap<String, String>): Call<ResponseBody>

    @FormUrlEncoded
    @POST("restaurants/add_booking_fooditemdetails")
    fun makeHomeDelivery(@Field("restaurant_id") resId: String, @Field("params") menuItems: String): Call<ResponseBody>

    @GET("api/restaurants/add_send_rest_token")
    fun sendToken(@Query("rest_noti") token: String, @Query("restaurant_id") rest_id: String): Call<ResponseBody>

    @GET("api/user/login_token")
    fun sendMyToken(@Query("token") token: String, @Query("mail") mail: String): Call<ResponseBody>
}
