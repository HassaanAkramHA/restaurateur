package com.dinepartner.restaurent.app.services

interface Result {

    fun onSuccess(data: String, requestCode: Int)
    fun onFailure(cause: String, requestCode: Int)
}