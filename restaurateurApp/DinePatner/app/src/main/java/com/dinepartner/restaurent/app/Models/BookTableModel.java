
package com.dinepartner.restaurent.app.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BookTableModel {

    @SerializedName("billing_last_name")
    @Expose
    private String billingLastName;
    @SerializedName("restaurateur_review_rating")
    @Expose
    private String restaurateurReviewRating;
    @SerializedName("discount")
    @Expose
    private String discount;
    @SerializedName("time_of_booking")
    @Expose
    private String timeOfBooking;
    @SerializedName("billing_mobile")
    @Expose
    private String billingMobile;
    @SerializedName("billing_address")
    @Expose
    private String billingAddress;
    @SerializedName("custom_note")
    @Expose
    private String customNote;
    @SerializedName("eventor_id")
    @Expose
    private String eventorId;
    @SerializedName("review_services")
    @Expose
    private String reviewServices;
    @SerializedName("billing_email")
    @Expose
    private String billingEmail;
    @SerializedName("review_overall")
    @Expose
    private String reviewOverall;
    @SerializedName("review_food_quality")
    @Expose
    private String reviewFoodQuality;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("payment_method")
    @Expose
    private String paymentMethod;
    @SerializedName("service_tax_amount")
    @Expose
    private String serviceTaxAmount;
    @SerializedName("transaction_id")
    @Expose
    private String transactionId;
    @SerializedName("price_per_head")
    @Expose
    private String pricePerHead;
    @SerializedName("hall_price")
    @Expose
    private String hallPrice;
    @SerializedName("hall_discount")
    @Expose
    private String hallDiscount;
    @SerializedName("bookirea_status")
    @Expose
    private String bookireaStatus;
    @SerializedName("booking_status")
    @Expose
    private String bookingStatus;
    @SerializedName("review_management")
    @Expose
    private String reviewManagement;
    @SerializedName("bookirea_commission")
    @Expose
    private String bookireaCommission;
    @SerializedName("decoration_price")
    @Expose
    private String decorationPrice;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("review_location")
    @Expose
    private String reviewLocation;
    @SerializedName("payment_complate_date")
    @Expose
    private Object paymentComplateDate;
    @SerializedName("date_of_booking")
    @Expose
    private String dateOfBooking;
    @SerializedName("occasion")
    @Expose
    private String occasion;
    @SerializedName("review_dispute_response")
    @Expose
    private Object reviewDisputeResponse;
    @SerializedName("restaurant_id")
    @Expose
    private String restaurantId;
    @SerializedName("seating_arrangment")
    @Expose
    private String seatingArrangment;
    @SerializedName("theme_id")
    @Expose
    private String themeId;
    @SerializedName("review_created_on")
    @Expose
    private String reviewCreatedOn;
    @SerializedName("notify")
    @Expose
    private String notify;
    @SerializedName("capacity")
    @Expose
    private String capacity;
    @SerializedName("gst_rate")
    @Expose
    private String gstRate;
    @SerializedName("seating_style")
    @Expose
    private String seatingStyle;
    @SerializedName("hall_id")
    @Expose
    private String hallId;
    @SerializedName("payment_id")
    @Expose
    private String paymentId;
    @SerializedName("service_tax_rate")
    @Expose
    private String serviceTaxRate;
    @SerializedName("review_comment")
    @Expose
    private String reviewComment;
    @SerializedName("review_status")
    @Expose
    private String reviewStatus;
    @SerializedName("billing_first_name")
    @Expose
    private String billingFirstName;
    @SerializedName("menu_id")
    @Expose
    private String menuId;
    @SerializedName("booking_trash")
    @Expose
    private String bookingTrash;
    @SerializedName("invoice_no")
    @Expose
    private String invoiceNo;
    @SerializedName("restaurateur_review_comments")
    @Expose
    private Object restaurateurReviewComments;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("service_tax_type")
    @Expose
    private String serviceTaxType;
    @SerializedName("cancel_reason")
    @Expose
    private String cancelReason;
    @SerializedName("review_cleaness")
    @Expose
    private String reviewCleaness;
    @SerializedName("gst_type")
    @Expose
    private String gstType;
    @SerializedName("booking_type")
    @Expose
    private String bookingType;
    @SerializedName("payable_amount")
    @Expose
    private String payableAmount;
    @SerializedName("number_of_head")
    @Expose
    private String numberOfHead;
    @SerializedName("review_dispute")
    @Expose
    private Object reviewDispute;

    public String getBillingLastName() {
        return billingLastName;
    }

    public void setBillingLastName(String billingLastName) {
        this.billingLastName = billingLastName;
    }

    public String getRestaurateurReviewRating() {
        return restaurateurReviewRating;
    }

    public void setRestaurateurReviewRating(String restaurateurReviewRating) {
        this.restaurateurReviewRating = restaurateurReviewRating;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getTimeOfBooking() {
        return timeOfBooking;
    }

    public void setTimeOfBooking(String timeOfBooking) {
        this.timeOfBooking = timeOfBooking;
    }

    public String getBillingMobile() {
        return billingMobile;
    }

    public void setBillingMobile(String billingMobile) {
        this.billingMobile = billingMobile;
    }

    public String getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
    }

    public String getCustomNote() {
        return customNote;
    }

    public void setCustomNote(String customNote) {
        this.customNote = customNote;
    }

    public String getEventorId() {
        return eventorId;
    }

    public void setEventorId(String eventorId) {
        this.eventorId = eventorId;
    }

    public String getReviewServices() {
        return reviewServices;
    }

    public void setReviewServices(String reviewServices) {
        this.reviewServices = reviewServices;
    }

    public String getBillingEmail() {
        return billingEmail;
    }

    public void setBillingEmail(String billingEmail) {
        this.billingEmail = billingEmail;
    }

    public String getReviewOverall() {
        return reviewOverall;
    }

    public void setReviewOverall(String reviewOverall) {
        this.reviewOverall = reviewOverall;
    }

    public String getReviewFoodQuality() {
        return reviewFoodQuality;
    }

    public void setReviewFoodQuality(String reviewFoodQuality) {
        this.reviewFoodQuality = reviewFoodQuality;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getServiceTaxAmount() {
        return serviceTaxAmount;
    }

    public void setServiceTaxAmount(String serviceTaxAmount) {
        this.serviceTaxAmount = serviceTaxAmount;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getPricePerHead() {
        return pricePerHead;
    }

    public void setPricePerHead(String pricePerHead) {
        this.pricePerHead = pricePerHead;
    }

    public String getHallPrice() {
        return hallPrice;
    }

    public void setHallPrice(String hallPrice) {
        this.hallPrice = hallPrice;
    }

    public String getHallDiscount() {
        return hallDiscount;
    }

    public void setHallDiscount(String hallDiscount) {
        this.hallDiscount = hallDiscount;
    }

    public String getBookireaStatus() {
        return bookireaStatus;
    }

    public void setBookireaStatus(String bookireaStatus) {
        this.bookireaStatus = bookireaStatus;
    }

    public String getBookingStatus() {
        return bookingStatus;
    }

    public void setBookingStatus(String bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    public String getReviewManagement() {
        return reviewManagement;
    }

    public void setReviewManagement(String reviewManagement) {
        this.reviewManagement = reviewManagement;
    }

    public String getBookireaCommission() {
        return bookireaCommission;
    }

    public void setBookireaCommission(String bookireaCommission) {
        this.bookireaCommission = bookireaCommission;
    }

    public String getDecorationPrice() {
        return decorationPrice;
    }

    public void setDecorationPrice(String decorationPrice) {
        this.decorationPrice = decorationPrice;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getReviewLocation() {
        return reviewLocation;
    }

    public void setReviewLocation(String reviewLocation) {
        this.reviewLocation = reviewLocation;
    }

    public Object getPaymentComplateDate() {
        return paymentComplateDate;
    }

    public void setPaymentComplateDate(Object paymentComplateDate) {
        this.paymentComplateDate = paymentComplateDate;
    }

    public String getDateOfBooking() {
        return dateOfBooking;
    }

    public void setDateOfBooking(String dateOfBooking) {
        this.dateOfBooking = dateOfBooking;
    }

    public String getOccasion() {
        return occasion;
    }

    public void setOccasion(String occasion) {
        this.occasion = occasion;
    }

    public Object getReviewDisputeResponse() {
        return reviewDisputeResponse;
    }

    public void setReviewDisputeResponse(Object reviewDisputeResponse) {
        this.reviewDisputeResponse = reviewDisputeResponse;
    }

    public String getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    public String getSeatingArrangment() {
        return seatingArrangment;
    }

    public void setSeatingArrangment(String seatingArrangment) {
        this.seatingArrangment = seatingArrangment;
    }

    public String getThemeId() {
        return themeId;
    }

    public void setThemeId(String themeId) {
        this.themeId = themeId;
    }

    public String getReviewCreatedOn() {
        return reviewCreatedOn;
    }

    public void setReviewCreatedOn(String reviewCreatedOn) {
        this.reviewCreatedOn = reviewCreatedOn;
    }

    public String getNotify() {
        return notify;
    }

    public void setNotify(String notify) {
        this.notify = notify;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public String getGstRate() {
        return gstRate;
    }

    public void setGstRate(String gstRate) {
        this.gstRate = gstRate;
    }

    public String getSeatingStyle() {
        return seatingStyle;
    }

    public void setSeatingStyle(String seatingStyle) {
        this.seatingStyle = seatingStyle;
    }

    public String getHallId() {
        return hallId;
    }

    public void setHallId(String hallId) {
        this.hallId = hallId;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getServiceTaxRate() {
        return serviceTaxRate;
    }

    public void setServiceTaxRate(String serviceTaxRate) {
        this.serviceTaxRate = serviceTaxRate;
    }

    public String getReviewComment() {
        return reviewComment;
    }

    public void setReviewComment(String reviewComment) {
        this.reviewComment = reviewComment;
    }

    public String getReviewStatus() {
        return reviewStatus;
    }

    public void setReviewStatus(String reviewStatus) {
        this.reviewStatus = reviewStatus;
    }

    public String getBillingFirstName() {
        return billingFirstName;
    }

    public void setBillingFirstName(String billingFirstName) {
        this.billingFirstName = billingFirstName;
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getBookingTrash() {
        return bookingTrash;
    }

    public void setBookingTrash(String bookingTrash) {
        this.bookingTrash = bookingTrash;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public Object getRestaurateurReviewComments() {
        return restaurateurReviewComments;
    }

    public void setRestaurateurReviewComments(Object restaurateurReviewComments) {
        this.restaurateurReviewComments = restaurateurReviewComments;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getServiceTaxType() {
        return serviceTaxType;
    }

    public void setServiceTaxType(String serviceTaxType) {
        this.serviceTaxType = serviceTaxType;
    }

    public String getCancelReason() {
        return cancelReason;
    }

    public void setCancelReason(String cancelReason) {
        this.cancelReason = cancelReason;
    }

    public String getReviewCleaness() {
        return reviewCleaness;
    }

    public void setReviewCleaness(String reviewCleaness) {
        this.reviewCleaness = reviewCleaness;
    }

    public String getGstType() {
        return gstType;
    }

    public void setGstType(String gstType) {
        this.gstType = gstType;
    }

    public String getBookingType() {
        return bookingType;
    }

    public void setBookingType(String bookingType) {
        this.bookingType = bookingType;
    }

    public String getPayableAmount() {
        return payableAmount;
    }

    public void setPayableAmount(String payableAmount) {
        this.payableAmount = payableAmount;
    }

    public String getNumberOfHead() {
        return numberOfHead;
    }

    public void setNumberOfHead(String numberOfHead) {
        this.numberOfHead = numberOfHead;
    }

    public Object getReviewDispute() {
        return reviewDispute;
    }

    public void setReviewDispute(Object reviewDispute) {
        this.reviewDispute = reviewDispute;
    }

}
