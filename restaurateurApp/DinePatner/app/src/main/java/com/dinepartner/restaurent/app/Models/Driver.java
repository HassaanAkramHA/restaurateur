package com.dinepartner.restaurent.app.Models;

public class Driver {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBikeNumber() {
        return bikeNumber;
    }

    public void setBikeNumber(String bikeNumber) {
        this.bikeNumber = bikeNumber;
    }

    public Driver(String name, String bikeNumber) {
        this.name = name;
        this.bikeNumber = bikeNumber;
    }

    private String bikeNumber;
}
