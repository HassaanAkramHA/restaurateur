package com.dinepartner.restaurent.app.services;

import com.dinepartner.restaurent.app.retrofitclientfortoken.ResponseBondy;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 *  * Created by shoaib on 8-02-2017.
 *  
 */
public interface FCMServices {
    @FormUrlEncoded
    @POST("android_push/android_rest_noti.php")
    Call<ResponseBondy> getNotification(@Field("token") String token);
}