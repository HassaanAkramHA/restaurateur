package com.dinepartner.restaurent.app.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.bumptech.glide.request.RequestOptions;
import com.dinepartner.restaurent.app.R;
import com.dinepartner.restaurent.app.helpers.Constants;
import com.dinepartner.restaurent.app.services.Result;
import com.dinepartner.restaurent.app.services.ServicesFactory;
import com.facebook.*;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import static com.dinepartner.restaurent.app.helpers.ExtentionsKt.moveTo;

public class Login extends AppCompatActivity implements Result, View.OnClickListener {
    private CallbackManager callbackManager;
    private LoginButton loginButton;
    Button facebookBtnClick, loginBtn;
    EditText emailEt, passwordEt;
    private Boolean isTypeEmail = true;
    private String emailOrPhone = "";
    private String password = "";
    CardView loadingProgressBar;
    Intent intent;
    SignInButton googleSignInBtn;
    TextView forgotpasswordTv, signUpNowTv;
    GoogleSignInClient mGoogleSignInClient;
    int RC_SIGN_IN = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        initView();

    }

    private void initView() {
        forgotpasswordTv = findViewById(R.id.forget_tv);
        forgotpasswordTv.setOnClickListener(this);
        signUpNowTv = findViewById(R.id.signUpnow_tv);
        signUpNowTv.setOnClickListener(this);
        intent = getIntent();
        emailEt = findViewById(R.id.email_input);
        passwordEt = findViewById(R.id.password_input);
        loginBtn = findViewById(R.id.login_btn);
        loginBtn.setOnClickListener(this);
        loadingProgressBar = findViewById(R.id.loadingViewLogin);

    }


    private Boolean areInputsOkay() {
        if (TextUtils.isEmpty(emailEt.getText())) {
            emailEt.setError("Required");
            emailEt.requestFocus();
            return false;
        } else if (TextUtils.isDigitsOnly(emailEt.getText())) {
            isTypeEmail = false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(emailEt.getText()).matches()) {
            isTypeEmail = true;
            emailEt.setError("Invalid Email");
            emailEt.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(passwordEt.getText())) {
            passwordEt.setError("Required");
            passwordEt.requestFocus();
            return false;
        }
        emailOrPhone = emailEt.getText().toString();
        password = passwordEt.getText().toString();
        return true;
    }


    @Override
    public void onSuccess(@NotNull String data, int requestCode) {
        loadingProgressBar.setVisibility(View.GONE);
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(getString(R.string.FCM_PREF), Context.MODE_PRIVATE);
        String token = sharedPreferences.getString(getString(R.string.FCM_TOKEN), "");

        if (requestCode == 0) {
            new ServicesFactory(this, 1).sendToken(token,emailOrPhone);

            if (intent.getStringExtra(Constants.Companion.getDataPassKey()) == null) {
                moveTo(this, MainActivity.class);
            } else {
                setResult(Activity.RESULT_OK);
                finish();
            }
            Toast.makeText(this, "success", Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(this, "success", Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public void onFailure(@NotNull String cause, int requestCode) {
        Toast.makeText(this, "failure", Toast.LENGTH_LONG).show();
        loadingProgressBar.setVisibility(View.GONE);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.login_btn:

                if (areInputsOkay()) {
                    loadingProgressBar.setVisibility(View.VISIBLE);
                    if (isTypeEmail) {

                        new ServicesFactory(this, 0).loginWithEmail(emailOrPhone, password);
                    } else
                        new ServicesFactory(this, 0).loginWithMobileNumber(emailOrPhone, password);
                }

                break;
            case R.id.forget_tv:
//                Intent intent = new Intent(this, ForgotPasswordActivity.class);
                startActivity(intent);
                break;
            case R.id.signUpnow_tv:
                Intent intents = new Intent(this, SignUpActivity.class);
                startActivity(intents);
                break;
        }

    }
}
