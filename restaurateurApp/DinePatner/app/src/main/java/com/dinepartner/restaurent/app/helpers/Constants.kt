package com.dinepartner.restaurent.app.helpers

class Constants {

    companion object {
        val dataPassKey = "dataPassKey"
        val additionalDataPassKey = "additionalDataPassKey"
        val restaurantLogoUrlPrefix = "https://dinepartner.com/img/restaurants/thumbs/"
        val cuisineLogoUrlPrefix = "https://dinepartner.com/img/uploads/"
        val photosUrlPrefix = "https://dinepartner.com/img/restaurants/"

        val SUCCESS_RESULT = 0
        val FAILURE_RESULT = 1
        val PACKAGE_NAME = "com.google.android.gms.location.sample.locationaddress"
        val RECEIVER = "$PACKAGE_NAME.RECEIVER"
        val RESULT_DATA_KEY = "$PACKAGE_NAME.RESULT_DATA_KEY"
        val LOCATION_DATA_EXTRA = "$PACKAGE_NAME.LOCATION_DATA_EXTRA"
    }
}