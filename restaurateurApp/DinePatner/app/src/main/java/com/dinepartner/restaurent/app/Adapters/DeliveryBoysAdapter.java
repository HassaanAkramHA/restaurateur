package com.dinepartner.restaurent.app.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.dinepartner.restaurent.app.Models.Driver;
import com.dinepartner.restaurent.app.Models.HomedeliveryModel;
import com.dinepartner.restaurent.app.R;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Objects;

public class DeliveryBoysAdapter extends ArrayAdapter<Driver> {
    private static class ViewHolder {
        TextView txtName;
        TextView txtBikeNumber;
    }

    private ArrayList<Driver> DriverList;
    private Context mContext;

    public DeliveryBoysAdapter(@NonNull Context context, ArrayList<Driver> DriverList) {
        super((context), R.layout.delivery_boys, DriverList);
        this.DriverList = DriverList;
        this.mContext = context;
    }

    private int lastPosition = -1;

    @NotNull
    @Override
    public View getView(int position, View convertView, @NotNull ViewGroup parent) {
        // Get the data item for this position
        // Check if an existing view is being reused, otherwise inflate the view
        Driver driver = getItem(position);

        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new DeliveryBoysAdapter.ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.delivery_boys, parent, false);
            viewHolder.txtName = (TextView) convertView.findViewById(R.id.user_name);
            viewHolder.txtBikeNumber = (TextView) convertView.findViewById(R.id.user_bike);

            result = convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (DeliveryBoysAdapter.ViewHolder) convertView.getTag();
            result = convertView;
        }

        Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        result.startAnimation(animation);
        lastPosition = position;

        viewHolder.txtName.setText(driver.getName());
        viewHolder.txtBikeNumber.setText((driver.getBikeNumber()));
        // Return the completed view to render on screen
        return convertView;
    }
}
