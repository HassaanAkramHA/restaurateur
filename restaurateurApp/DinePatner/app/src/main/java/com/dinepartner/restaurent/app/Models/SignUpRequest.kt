package com.dinepartner.restaurent.app.Models

import android.os.Parcel
import android.os.Parcelable
import com.google.firebase.auth.PhoneAuthProvider

class SignUpRequest(
    var name: String,
    var email: String,
    var password: String,
    var contact: String,

    var verificationId: String, var token: PhoneAuthProvider.ForceResendingToken?, var dateAndTime:String, var messagingToken:String
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readParcelable(PhoneAuthProvider.ForceResendingToken::class.java.classLoader),
        parcel.readString()!!,
        parcel.readString()!!

        )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(email)
        parcel.writeString(password)
        parcel.writeString(contact)
        parcel.writeString(verificationId)
        parcel.writeParcelable(token, flags)
        parcel.writeString(dateAndTime)
        parcel.writeString(messagingToken)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SignUpRequest> {
        override fun createFromParcel(parcel: Parcel): SignUpRequest {
            return SignUpRequest(parcel)
        }

        override fun newArray(size: Int): Array<SignUpRequest?> {
            return arrayOfNulls(size)
        }
    }

}