package com.dinepartner.restaurent.app.Models

import androidx.annotation.NonNull
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class User(
    @NonNull
    @SerializedName("user_id")
    @Expose
    var userId: String,
    @SerializedName("email")
    @Expose
    var email: String,
    @SerializedName("password")
    @Expose
    var password: String,
    @SerializedName("token")
    @Expose
    var token: String,
    @SerializedName("first_name")
    @Expose
    var firstName: String,
    @SerializedName("last_name")
    @Expose
    var lastName: String,
    @SerializedName("contact")
    @Expose
    var contact: String,
    @SerializedName("city_id")
    @Expose
    var cityId: String,
    @SerializedName("status")
    @Expose
    var status: String,
    @SerializedName("privelages")
    @Expose
    var privelages: String,
    @SerializedName("parent_id")
    @Expose
    var parentId: String,
    @SerializedName("created")
    @Expose
    var created: String,
    @SerializedName("created_by")
    @Expose
    var createdBy: String,
    @SerializedName("type")
    @Expose
    var type: String,
    @SerializedName("user_type")
    @Expose
    var userType: String,
    @SerializedName("order_type")
    @Expose
    var orderType: String,
    @SerializedName("profile_pic")
    @Expose
    var profilePic: String,
    @SerializedName("gender")
    @Expose
    var gender: String,
    @SerializedName("trash")
    @Expose
    var trash: String,
    @SerializedName("bank_name")
    @Expose
    var bankName: String,
    @SerializedName("account_title")
    @Expose
    var accountTitle: String,
    @SerializedName("account_no")
    @Expose
    var accountNo: String,
    @SerializedName("sort_code")
    @Expose
    var sortCode: String,
    @SerializedName("branch_code")
    @Expose
    var branchCode: String,
    @SerializedName("bank_bic")
    @Expose
    var bankBic: String,
    @SerializedName("bank_iban")
    @Expose
    var bankIban: String,
    @SerializedName("contact_name")
    @Expose
    var contactName: String,
    @SerializedName("contact_address")
    @Expose
    var contactAddress: String,
    @SerializedName("contact_city")
    @Expose
    var contactCity: String,
    @SerializedName("contact_region")
    @Expose
    var contactRegion: String,
    @SerializedName("contact_zipcode")
    @Expose
    var contactZipcode: String,
    @SerializedName("contact_number")
    @Expose
    var contactNumber: String,
    @SerializedName("tnc_flag")
    @Expose
    var tncFlag: String,
    @SerializedName("reference")
    @Expose
    var reference: String,
    @SerializedName("google_auth_uid")
    @Expose
    var googleAuthUid: String,
    @SerializedName("facebook_auth_uid")
    @Expose
    var facebookAuthUid: String,
    @SerializedName("otp")
    @Expose
    var otp: String,
    @SerializedName("latitude")
    @Expose
    var latitude: String?,
    @SerializedName("longitude")
    @Expose
    var longitude: String?,
    @SerializedName("delivery_address1")
    @Expose
    var deliveryAddress1: String?,
    @SerializedName("delivery_address2")
    @Expose
    var deliveryAddress2: String?,
    @SerializedName("user_pic")
    @Expose
    var userPic: String?

)