package com.dinepartner.restaurent.app.Adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dinepartner.restaurent.app.Models.HomedeliveryModel;
import com.dinepartner.restaurent.app.R;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;


public class ItemsAdapter extends ArrayAdapter<HomedeliveryModel> {


    private ArrayList<HomedeliveryModel> dataSet;
   private Context mContext;

    // View lookup cache
    private static class ViewHolder {
        TextView txtName;
    }

    public ItemsAdapter(ArrayList<HomedeliveryModel> data, Context context) {
        super(context, R.layout.adapter_list_item, data);
        this.dataSet = data;
        this.mContext = context;

    }

    private int lastPosition = -1;

    @NotNull
    @Override
    public View getView(int position, View convertView, @NotNull ViewGroup parent) {
        // Get the data item for this position
        HomedeliveryModel HomedeliveryModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.adapter_list_item, parent, false);
            viewHolder.txtName = (TextView) convertView.findViewById(R.id.name_item);

            result = convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result = convertView;
        }

        Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        result.startAnimation(animation);
        lastPosition = position;

        viewHolder.txtName.setText(HomedeliveryModel.getTitle());
        // Return the completed view to render on screen
        return convertView;
    }
}