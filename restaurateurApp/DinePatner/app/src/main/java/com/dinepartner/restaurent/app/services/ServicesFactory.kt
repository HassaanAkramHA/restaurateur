package com.dinepartner.restaurent.app.services

import com.dinepartner.restaurent.app.Models.SignUpRequest
import com.dinepartner.restaurent.app.helpers.createMd5
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception

class ServicesFactory(private val resultCallback: Result, private val requestCode: Int) : Callback<ResponseBody> {


    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
        if (response.isSuccessful) {
            try {
                val jsonObject = JSONObject(response.body()!!.string())
                if (jsonObject.getString("status") == "success")
                    resultCallback.onSuccess(jsonObject.get("data").toString(), requestCode)
                else
                    resultCallback.onFailure(jsonObject.get("data").toString(), requestCode)
            } catch (e: Exception) {
                e.printStackTrace()
                resultCallback.onFailure(e.message ?: "Something went wrong", requestCode)
            }
        } else {
            resultCallback.onFailure("Something went wrong", requestCode)
        }
    }

    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
        resultCallback.onFailure(t.message ?: "Something went wrong", requestCode)
    }

    fun loginWithEmail(email: String, password: String) {
        val encryptedPassword = createMd5(password)
        RetrofitClient.getInstance().create(EndPoints::class.java).loginWithEmail(email, encryptedPassword)
                .enqueue(this)
    }

    fun loginWithMobileNumber(mobileNumber: String, password: String) {
        val encryptedPassword = createMd5(password)
        RetrofitClient.getInstance().create(EndPoints::class.java)
                .loginWithMobileNumber(mobileNumber, encryptedPassword)
                .enqueue(this)
    }
    fun getAllCuisines() {
        RetrofitClient.getInstance().create(EndPoints::class.java).getAllCuisines().enqueue(this)
    }

    fun sendTokenToServer(token: String, rest_id: String) {
        RetrofitClient.getInstance().create(EndPoints::class.java).sendToken(token,rest_id).enqueue(this)
    }


    fun getFeaturedRestaurants(cityId: String) {
        RetrofitClient.getInstance().create(EndPoints::class.java).getFeaturedRestaurants(cityId).enqueue(this)
    }

    fun getAllRestaurants() {
        RetrofitClient.getInstance().create(EndPoints::class.java).getAllRestaurants().enqueue(this)
    }

    fun getNearByRestaurants(lat: Double, lon: Double) {
        RetrofitClient.getInstance().create(EndPoints::class.java).getNearByRestaurants(lat, lon).enqueue(this)
    }

    fun getRestaurantsByOptions(name: String, dateTime: String, persons: Int) {
        val date = dateTime.split("|")[0]
        val time = dateTime.split("|")[1]
        RetrofitClient.getInstance().create(EndPoints::class.java)
                .getRestaurantsByOptions(name, date, time, persons.toString()).enqueue(this)
    }

    fun getRestaurantImages(restaurantSlug: String) {
        RetrofitClient.getInstance().create(EndPoints::class.java).getRestaurantImages(restaurantSlug).enqueue(this)
    }

    fun getRestaurantReview(restaurantId: String) {
        RetrofitClient.getInstance().create(EndPoints::class.java).getRestaurantReview(restaurantId).enqueue(this)
    }

    fun getRestaurantMenu(restaurantId: String) {
        RetrofitClient.getInstance().create(EndPoints::class.java).getRestaurantMenu(restaurantId).enqueue(this)
    }

    fun getRestaurantDetails(restaurantId: String) {
        RetrofitClient.getInstance().create(EndPoints::class.java).getRestaurantDetails(restaurantId).enqueue(this)
    }

    fun checkBooking(userId: String, restaurantId: String, persons: String, date: String, time: String) {
        val queryMap: HashMap<String, String> = HashMap()
        queryMap["user_id"] = userId
        queryMap["restaurant_id"] = restaurantId
        queryMap["booking_capacity"] = persons
        queryMap["date"] = date
        queryMap["time"] = time
        RetrofitClient.getInstance().create(EndPoints::class.java).checkBooking(queryMap).enqueue(this)
    }

    fun getHomeDelivery(restaurantId: String, menuItem: String) {
        RetrofitClient.getInstance().create(EndPoints::class.java).makeHomeDelivery(restaurantId, menuItem).enqueue(this)
    }
    fun sendToken(token:String,mail:String){
        RetrofitClient.getInstance().create(EndPoints::class.java).sendMyToken(token,mail).enqueue(this)
    }
    fun signUp(signUpRequest: SignUpRequest,token: String) {
        val names = signUpRequest.name.split(" ")
        val firstName = names[0]
        var lastName = ""
        if (names.size > 1)
            lastName = names[names.size - 1]

        val encryptedPassword = createMd5(signUpRequest.password)

        RetrofitClient.getInstance().create(EndPoints::class.java)
                .signUp(firstName, lastName, signUpRequest.email, encryptedPassword, signUpRequest.contact,signUpRequest.dateAndTime,token)
                .enqueue(this)
    }
}