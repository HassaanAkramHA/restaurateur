package com.dinepartner.restaurent.app.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.os.Bundle;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.dinepartner.restaurent.app.Adapters.ItemsAdapter;
import com.dinepartner.restaurent.app.MapActivity;
import com.dinepartner.restaurent.app.Models.BookTableModel;
import com.dinepartner.restaurent.app.Models.HomedeliveryModel;
import com.dinepartner.restaurent.app.R;
import com.dinepartner.restaurent.app.services.FCMServices;
import com.dinepartner.restaurent.app.services.Result;
import com.dinepartner.restaurent.app.services.ServicesFactory;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import static com.dinepartner.restaurent.app.R.string.msg_token_fmt;


public class MainActivity extends AppCompatActivity implements View.OnClickListener, Result, NavigationView.OnNavigationItemSelectedListener {

    Button acceptButton;
    Button rejectButton;
    FCMServices fmcNotificationService;
    String token;
    Intent intent;
    Gson gson = new Gson();
    BookTableModel bookTableModel;
    ListView listView;
    ArrayList<HomedeliveryModel> homedeliveryModels = new ArrayList<HomedeliveryModel>();
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        DrawerLayout drawer = findViewById(R.id.drawer);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        listView = findViewById(R.id.list_of_items);
        intent = getIntent();
        if (intent.getExtras() != null) {
            try {
                JSONArray arrayBookTable = new JSONArray(getIntent().getExtras().getString("moredata"));
//                JSONArray arrayForHomeDelivery = new JSONArray(getIntent().getExtras().getString("moredata2"));
                bookTableModel = gson.fromJson(arrayBookTable.getJSONObject(0).toString(), BookTableModel.class);
//                homedeliveryModels = gson.fromJson(arrayForHomeDelivery.toString(), new TypeToken<ArrayList<HomedeliveryModel>>() {
//                }.getType());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        ItemsAdapter itemsAdapter = new ItemsAdapter(homedeliveryModels, this);
        listView.setAdapter(itemsAdapter);
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w("taga", "getInstanceId failed", task.getException());
                            return;
                        }
                        String token = task.getResult().getToken();
                        @SuppressLint({"StringFormatInvalid", "LocalSuppress"}) String msg = getString(msg_token_fmt, token);
                        Log.d("tagb", msg);
                        Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                });


        acceptButton = findViewById(R.id.accept_btn);
        acceptButton.setOnClickListener(this);

        rejectButton = findViewById(R.id.reject_btn);
        rejectButton.setOnClickListener(this);


    }


    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.accept_btn:
                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(getString(R.string.FCM_PREF), Context.MODE_PRIVATE);
                token = sharedPreferences.getString(getString(R.string.FCM_TOKEN), "");
//                new ServicesFactory(this, 0).sendTokenToServer(token, "2");
                break;
            case R.id.reject_btn:
                Intent intent1 = new Intent(MainActivity.this, Signup.class);
                startActivity(intent1);
                break;
        }

    }

    @Override
    public void onSuccess(@NotNull String data, int requestCode) {

        Toast.makeText(this, data, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure(@NotNull String cause, int requestCode) {
        Toast.makeText(this, cause, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        // Handle navigation view item clicks here.
        int id = menuItem.getItemId();
        if (id == R.id.nav_home) {
            menuItem.setCheckable(true);
            menuItem.setChecked(true);
        } else if (id == R.id.nav_job_record) {
            Intent intent = new Intent(MainActivity.this, JobRecorsActivity.class);
            startActivity(intent);
        } else if (id == R.id.Settings) {
            Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_map) {
            Intent intent = new Intent(MainActivity.this, MapActivity.class);
            startActivity(intent);
        } else if (id == R.id.delivery_boys) {
            Intent intent = new Intent(MainActivity.this, DeliveryBoysActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = findViewById(R.id.drawer);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
