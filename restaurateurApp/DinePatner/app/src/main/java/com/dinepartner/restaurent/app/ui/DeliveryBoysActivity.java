package com.dinepartner.restaurent.app.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;

import com.dinepartner.restaurent.app.Adapters.DeliveryBoysAdapter;
import com.dinepartner.restaurent.app.Models.Driver;
import com.dinepartner.restaurent.app.R;

import java.util.ArrayList;

public class DeliveryBoysActivity extends AppCompatActivity {

    ArrayList<String> namesList = new ArrayList<String>();
    ArrayList<String> BikeList = new ArrayList<String>();
    ArrayList<Integer> imagesList = new ArrayList<Integer>();
    ListView listView;
    ArrayList<Driver> driversList = new ArrayList<Driver>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_boys);
        initList();
        DeliveryBoysAdapter deliveryBoysAdapter = new DeliveryBoysAdapter(this, driversList);
        listView.setAdapter(deliveryBoysAdapter);
    }

    private void initList() {
        listView = findViewById(R.id.delivery_boys_list);

        namesList.add("ann");
        namesList.add("john");
        namesList.add("pop");
        namesList.add("De silva");
        namesList.add("Dhananjya");
        namesList.add("Chandimal");
        namesList.add("Bony kapoor");
        namesList.add("Shahid kamal");
        namesList.add("Boby khan");
        namesList.add("Nawaz sharif");
        namesList.add("Imran khan");
        namesList.add("Chawdhry sarwar");
        namesList.add("john smith");
        namesList.add("john kapoor");
        namesList.add("john panday");
        BikeList.add("BKL 2020");
        BikeList.add("BKL 2020");
        BikeList.add("BKL 2020");
        BikeList.add("BKL 2020");
        BikeList.add("BKL 2020");
        BikeList.add("BKL 2020");
        BikeList.add("BKL 2020");
        BikeList.add("BKL 2020");
        BikeList.add("BKL 2020");
        BikeList.add("BKL 2020");
        BikeList.add("BKL 2020");
        BikeList.add("BKL 2020");
        BikeList.add("BKL 2020");
        BikeList.add("BKL 2020");
        BikeList.add("BKL 2020");
        for (int i = 0; i < namesList.size(); i++) {
            Driver driver = new Driver(namesList.get(i), BikeList.get(i));
            driversList.add(driver);
        }

    }
}
