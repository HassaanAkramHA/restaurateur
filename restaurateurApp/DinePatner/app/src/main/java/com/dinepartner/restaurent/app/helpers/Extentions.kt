package com.dinepartner.restaurent.app.helpers

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.dinepartner.restaurent.app.R
import java.security.NoSuchAlgorithmException


fun AppCompatActivity.moveTo(className: Class<*>) {
    startActivity(Intent(this, className))
    overridePendingTransition(R.anim.activity_enter, R.anim.activity_stay)
}

fun AppCompatActivity.moveTo(intent: Intent) {
    startActivity(intent)
    overridePendingTransition(R.anim.activity_enter, R.anim.activity_stay)
}

fun Fragment.moveTo(activity: AppCompatActivity, className: Class<*>) {
    startActivity(Intent(activity, className))
    activity.overridePendingTransition(R.anim.activity_enter, R.anim.activity_stay)
}

fun Fragment.moveTo(activity: AppCompatActivity, intent: Intent) {
    startActivity(intent)
    activity.overridePendingTransition(R.anim.activity_enter, R.anim.activity_stay)
}

fun Fragment.startForResult(activity: AppCompatActivity, intent: Intent, requestCode: Int) {
    startActivityForResult(intent, requestCode)
    activity.overridePendingTransition(R.anim.activity_enter, R.anim.activity_stay)
}

fun AppCompatActivity.hasLocationPermission(): Boolean {
    return ContextCompat.checkSelfPermission(
        this,
        Manifest.permission.ACCESS_FINE_LOCATION
    ) == PackageManager.PERMISSION_GRANTED
            && ContextCompat.checkSelfPermission(
        this,
        Manifest.permission.ACCESS_COARSE_LOCATION
    ) == PackageManager.PERMISSION_GRANTED
}

fun AppCompatActivity.requestLocationPermission() {
    ActivityCompat.requestPermissions(
        this,
        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION),
        0
    )
}

fun Fragment.hasLocationPermission(context: Context): Boolean {
    return ContextCompat.checkSelfPermission(
        context,
        Manifest.permission.ACCESS_FINE_LOCATION
    ) == PackageManager.PERMISSION_GRANTED
            && ContextCompat.checkSelfPermission(
        context,
        Manifest.permission.ACCESS_COARSE_LOCATION
    ) == PackageManager.PERMISSION_GRANTED
}

fun Fragment.requestLocationPermission() {
    requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION), 0)
}

fun createMd5(s: String): String {
    val MD5 = "MD5"
    try {
        // Create MD5 Hash
        val digest = java.security.MessageDigest
            .getInstance(MD5)
        digest.update(s.toByteArray())
        val messageDigest = digest.digest()

        // Create Hex String
        val hexString = StringBuilder()
        for (aMessageDigest in messageDigest) {
            var h = Integer.toHexString(0xFF and aMessageDigest.toInt())
            while (h.length < 2)
                h = "0$h"
            hexString.append(h)
        }
        return hexString.toString()

    } catch (e: NoSuchAlgorithmException) {
        e.printStackTrace()
    }

    return ""
}