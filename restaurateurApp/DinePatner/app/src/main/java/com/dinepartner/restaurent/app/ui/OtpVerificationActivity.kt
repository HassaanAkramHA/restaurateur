package com.dinepartner.restaurent.app.ui

import android.content.Context
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.dinepartner.restaurent.app.Models.SignUpRequest
import com.dinepartner.restaurent.app.R
import com.dinepartner.restaurent.app.helpers.Constants
import com.dinepartner.restaurent.app.helpers.moveTo
import com.dinepartner.restaurent.app.services.Result
import com.dinepartner.restaurent.app.services.ServicesFactory
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.FirebaseException
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import com.google.firebase.iid.FirebaseInstanceId
import kotlinx.android.synthetic.main.activity_otp_verification.*
import kotlinx.android.synthetic.main.activity_otp_verification.loadingView
import kotlinx.android.synthetic.main.signup.*
import java.util.concurrent.TimeUnit


@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class OtpVerificationActivity : AppCompatActivity(), Result, OnCompleteListener<AuthResult> {

    private lateinit var signUpRequest: SignUpRequest
    var tokens = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otp_verification)
        supportActionBar?.title = "OTP Verification"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        signUpRequest = intent.getParcelableExtra(Constants.dataPassKey)
        signUpBtn.setOnClickListener {
            if (TextUtils.isEmpty(otpEt.text)) {
                otpEt.error = getString(R.string.required)
                otpEt.requestFocus()
                return@setOnClickListener
            }
            loadingView.visibility = View.VISIBLE
            val firebaseAuth = FirebaseAuth.getInstance()
            val credential = PhoneAuthProvider.getCredential(signUpRequest.verificationId, otpEt.text.toString())
            firebaseAuth.signInWithCredential(credential).addOnCompleteListener(this)

        }
    }

    var token = ""
    override fun onComplete(task: Task<AuthResult>) {
        if (task.isSuccessful) {

            val sharedPreferences = applicationContext.getSharedPreferences(getString(R.string.FCM_PREF), Context.MODE_PRIVATE)
            token = sharedPreferences.getString(getString(R.string.FCM_TOKEN), "")
            ServicesFactory(this@OtpVerificationActivity, 0).signUp(signUpRequest, "")
        } else {
            otpEt.error = "Invalid Code!"
            loadingView.visibility = View.GONE
        }
    }

    override fun onSuccess(data: String, requestCode: Int) {
        loadingView.visibility = View.GONE
        moveTo(Login::class.java)
    }

    override fun onFailure(cause: String, requestCode: Int) {
        loadingView.visibility = View.GONE
        Toast.makeText(this, cause, Toast.LENGTH_LONG).show()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        onBackPressed()
        return super.onOptionsItemSelected(item)
    }
}